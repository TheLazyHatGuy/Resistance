# Resistance
By Ducksworth Studios

## Roles

- Andrew Carolan
    - Lead Programmer
    - Level Design
- Aaron Richardson
    - Lead Sound Designer
    - Assistant Level Designer
- Cameron Scholes
    - Lead 3D Modelling
    - Assistant Gameplay Programmer
- Tomas Smith
    - Lead Gameplay Programmer
    - Assistant Programmer
    
## Gameplay
Resistance is a puzzle based escape room. 
The player must solve 2 different puzzles 
in order to unlock the door.
